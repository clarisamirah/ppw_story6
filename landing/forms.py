from django import forms

class formPost(forms.Form):
    description_attrs = {
        'class' : 'form-control h-100 d-inline-block',
        'name' : 'postingfield',
        'id' : 'exampleFormControlTextarea1',
        'type': 'text',
        'cols': 25,
        'rows': 40,
        'placeholder':'Masukkan ceritamu...'
    }

    posting = forms.CharField(label='', required=False, max_length=300, widget=forms.TextInput(attrs=description_attrs))
