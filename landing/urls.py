from django.conf.urls import url
from .views import index
from .views import add_post
from .views import profil
from .views import buku
from .views import datas
from .views import register, logout, substractCounter, addCounter
from .views import email_availability, registered_data, subscriber_list, deletion

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'add_post', add_post, name='add_post'),
	url(r'profil', profil, name='profil'),
	url(r'buku', buku, name='buku'),
	url(r'email-availability', email_availability, name = 'email-availability'),
	url(r'registereddata', registered_data, name = 'registereddata'),
	url(r'register', register, name='register'),
	url(r'data', datas, name = 'datas'),
	url(r'subscriber-list', subscriber_list, name = 'subscriber-list' ),
	url(r'substract', substractCounter, name = 'substract'),
	url(r'add', addCounter, name = 'add'),
	url(r'deletion', deletion, name = 'deletion'),
	url(r'logout', logout, name='logout'),
]