from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import add_post
from .views import profil, register, email_availability, subscriber_list, registered_data, deletion
from .models import myPosts, registered
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Story6UnitTest(TestCase):
	def test_story6_url_exists(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
	def test_story_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)
	def test_story_using_add_post_func(self):
		found = resolve('/add_post')
		self.assertEqual(found.func, add_post)
	def test_story6_uses_story6temp_as_template_(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'story6temp.html')
	def test_model_can_create_new_post(self):
		new_post = myPosts.objects.create(post='testcase')
		counting_all_available_post = myPosts.objects.all().count()
		self.assertEqual(counting_all_available_post, 1)
	def test_story6_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/add_post', {'posting': test})
		self.assertEqual(response_post.status_code, 302)
		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)
	def test_story6_get_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().get('/add_post', {'posting': ''})
		self.assertEqual(response_post.status_code, 302)
		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)
	def test_does_keyword_exist(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn('Hello apa kabar?', html_response)
	def test_challenge6_url_exists(self):
		response = Client().get('/profil/')
		self.assertEqual(response.status_code, 200)
	def test_challenge_using_profil_func(self):
		found = resolve('/profil')
		self.assertEqual(found.func, profil)
	def test_challenge6_uses_story6temp_as_template(self):
		response = Client().get('/profil/')
		self.assertTemplateUsed(response, 'story6prof.html')
	def test_doesinfos_keyword_exist(self):
		response = Client().get('/profil/')
		html_response = response.content.decode('utf8')
		response_infos = ["Nama : Clarisa Mirah Sekarsari", "NPM : 1706984530", "Umur : 18 tahun", "Jurusan : Sistem Informasi", "Angkatan : 2017", "Tanggal Lahir : 14 Februari 2000"]
		for e in range(len(response_infos)):
			self.assertIn(response_infos[e], html_response)
	def test_lab9_uses_story9bookshelf_as_template(self):
		response = Client().get('/buku/')
		self.assertTemplateUsed(response, 'story9bookshelf.html')
	def test_lab9_url_exists(self):
		response = Client().get('/buku/')
		self.assertEqual(response.status_code, 200)
	def test_json_data_url_exists(self):
		response = Client().get('/data/')
		self.assertEqual(response.status_code, 200)
	def test_story10_regis_url_exists(self):
		response = Client().get('/register/')
		self.assertEqual(response.status_code, 200)
	def test_story10_availability_url_exists(self):
		response = Client().get('/email-availability/?address=testtest@gmail.com/')
		self.assertEqual(response.status_code, 200)
	def test_story10_using_register_func(self):
		found = resolve('/register')
		self.assertEqual(found.func, register)
	def test_story10_using_availability_func(self):
		found = resolve('/email-availability/?address=testtest@gmail.com')
		self.assertEqual(found.func, email_availability)
	def test_story10_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/register', {'name': 'test', 'email' : 'testtest@gmail.com', 'password' : 'whahaha'})
		self.assertEqual(response_post.status_code, 200)
	def test_story10_post_fail_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/register', {'name': '', 'email' : '', 'password' : ''})
		self.assertEqual(response_post.status_code, 400)
	def test_story10_model_can_create_new_registered(self):
		new_registered = registered.objects.create(nama='test', email = 'testtest@gmail.com', password='whahaha')
		counting_all_registered = registered.objects.all().count()
		self.assertEqual(counting_all_registered, 1)
	def test_json_regisdata_url_exists(self):
		response = Client().get('/registereddata/')
		self.assertEqual(response.status_code, 200)
	def test_listsubs_url_exists(self):
		response = Client().get('/subscriber-list/')
		self.assertEqual(response.status_code, 200)
	def test_deletion_url_exists(self):
		response = Client().get('/deletion/')
		self.assertEqual(response.status_code, 302)
	def test_story10_using_subslist_func(self):
		found = resolve('/subscriber-list')
		self.assertEqual(found.func, subscriber_list)
	def test_story10_using_registereddata_func(self):
		found = resolve('/registereddata')
		self.assertEqual(found.func, registered_data)
	def test_story10_using_register_func(self):
		found = resolve('/deletion')
		self.assertEqual(found.func, deletion)
	

class Lab7FunctionalTest(TestCase) :
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab7FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Lab7FunctionalTest, self).tearDown()

	def test_input_post(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		to_post = selenium.find_element_by_id('exampleFormControlTextarea1')
		submit = selenium.find_element_by_id('submit')
		to_post.send_keys('Coba Coba')
		submit.send_keys(Keys.RETURN)
		self.assertIn('Coba Coba', selenium.page_source)
	def test_layout_1_titlelanding(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		self.assertIn('Landing Page', selenium.title)
	def test_layout_2_anchor(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		anchor_text = selenium.find_element_by_tag_name('a').text
		self.assertIn('Hello apa kabar?', anchor_text)
	def test_style_1_background_for_Landing_Page (self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		body_style = selenium.find_element_by_tag_name('body')
		attribute = body_style.get_attribute('style')
		self.assertIn("background-image: linear-gradient(to right bottom, lightblue, white);", attribute)
	def test_style_2_background_for_Profile_Page (self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/profil/')
		body_style = selenium.find_element_by_tag_name('body')
		attribute = body_style.get_attribute('style')
		self.assertIn("background-image: linear-gradient(to right bottom, lightblue, white);", attribute)
	def test_story8_layout_button_accordion_text(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/profil')
		acc_text = selenium.find_element_by_class_name('accordion').text
		self.assertIn('Aktivitas', acc_text)

		





