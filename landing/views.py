from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from .forms import formPost
from .models import myPosts
from .models import registered
import requests
from django.contrib.auth import logout as logoutImported
from django.core import serializers
# Create your views here.
response = {}
response['counter'] = 0
infos = ["Nama : Clarisa Mirah Sekarsari", "NPM : 1706984530", "Umur : 18 tahun", "Jurusan : Sistem Informasi", "Angkatan : 2017", "Tanggal Lahir : 14 Februari 2000"]
def index(request):    
    response['author'] = "" #TODO Implement yourname
    posts = myPosts.objects.all()
    response['posts'] = posts
    html = 'story6temp.html'
    response['formPost'] = formPost
    return render(request, html, response)
def add_post(request):
    form = formPost(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['posting'] = request.POST['posting']
        my_post = myPosts(post=response['posting'])
        my_post.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
def profil(request):
	response['infos'] = infos;
	return render(request, 'story6prof.html', response)
def buku(request):
    if request.user.is_authenticated:
        if 'counter' not in request.session :
            request.session['counter'] = 0
        response['counter'] = request.session['counter']
        if 'name' not in request.session :
            request.session['name'] = request.user.username
        if 'email' not in request.session :
            request.session['email'] = request.user.email
    return render(request, 'story9bookshelf.html', response)
def datas(request):
    txt = request.GET.get('cari','quilting')
    url = "https://www.googleapis.com/books/v1/volumes?q=" + txt
    dts = requests.get(url).json()
    return JsonResponse(dts)
def register(request):
    if(request.method == "POST"):
        data = dict(request.POST.items())
        tamu = registered(nama = data['name'],email = data['email'],password = data['password'])
        try:
            tamu.full_clean()
            tamu.save()
        except Exception as e:
            print(e.message_dict.items())
            e = [v[0] for k, v in e.message_dict.items()]
            return JsonResponse({"error": e}, status = 400)
        return JsonResponse({"message":"Data berhasil disimpan"})
    else:
        return render(request, 'story10regist.html', {})
def email_availability(request):
    email = request.GET.get("address")
    emails = registered.objects.filter(email=email).all()
    return JsonResponse({"email": email, "available": not bool(emails)})
def registered_data(request):
    dt = registered.objects.all().values('nama', 'email', 'password')
    dataReg = list(dt)
    return JsonResponse(dataReg, safe = False)
def subscriber_list(request):
    return render(request, 'story10list.html', response)
def deletion(request):
    if(request.method == 'POST'):
        data = dict(request.POST.items())
        registered.objects.filter(password = data['password']).delete()
        return JsonResponse({"message":"Data berhasil dihapus"})
    else :
        return HttpResponseRedirect('/subscriber-list/')
def logout(request):
    request.session.flush()
    logoutImported(request)
    return HttpResponseRedirect('/buku')
def addCounter(request):
    if request.user.is_authenticated :
        request.session['counter'] = request.session['counter'] + 1
    else:
        pass
    return HttpResponse(request.session['counter'], content_type = 'application/json')
def substractCounter(request):
    if request.user.is_authenticated :
        request.session['counter'] = request.session['counter'] - 1
    else:
        pass
    return HttpResponse(request.session['counter'], content_type = 'application/json')
