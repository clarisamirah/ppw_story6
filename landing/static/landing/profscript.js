function nightTheme() {
	for(i=0;i<3;i++){
		document.getElementsByClassName('accordion')[i].style.backgroundColor ='#3b4c49';
		document.getElementsByClassName('accordion')[i].style.color ='#fff';
		document.getElementsByClassName('accordion')[i].onmouseenter = function() 
		{
   			this.style.backgroundColor = "darkcyan";
   			this.style.color = '#fff'
		}
		document.getElementsByClassName('accordion')[i].onmouseleave = function() 
		{
   			this.style.backgroundColor = "#3b4c49";
   			this.style.color = '#fff'
		}
		
		document.getElementsByClassName('panel')[i].style.backgroundColor = '#7c9393';
	}
	document.getElementsByTagName('nav')[0].setAttribute('class', 'navbar navbar-expand-lg navbar-dark bg-dark text-white');
	document.body.style.backgroundImage = 'linear-gradient(to bottom right, darkcyan, black)';
}
function defaultTheme() {
	/*var acc = document.getElementsByClassName.('accordion');
	var navbar = document.getElementsByTagName('nav');*/
	for(i=0;i<3;i++){
		document.getElementsByClassName('accordion')[i].style.backgroundColor ='#b1d8d4';
		document.getElementsByClassName('accordion')[i].style.color ='#444';
		document.getElementsByClassName('accordion')[i].onmouseenter = function() 
		{
   			this.style.backgroundColor = "#79b2ab";
   			this.style.color = '#fff'
		}
		document.getElementsByClassName('accordion')[i].onmouseleave = function() 
		{
   			this.style.backgroundColor = "#b1d8d4";
   			this.style.color = '#fff'
		}
		document.getElementsByClassName('panel')[i].style.backgroundColor = '#e2ecff';
	}
	document.getElementsByTagName('nav')[0].setAttribute('class', 'navbar navbar-expand-lg navbar-light bg-info text-white');
	document.body.style.backgroundImage = 'linear-gradient(to bottom right, lightblue, white)';
}
